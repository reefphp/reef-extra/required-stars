<?php

namespace ReefExtra\RequiredStars;

use \Reef\Extension\Extension;

/**
 * Add required stars
 */
class RequiredStars extends Extension {
	
	/**
	 * @inherit
	 */
	public static function getName() : string {
		return 'reef-extra:required-stars';
	}
	
	/**
	 * @inherit
	 */
	public static function getDir() : string {
		return __DIR__.'/';
	}
	
	/**
	 * @inherit
	 */
	public function getCSS() : array {
		return [
			[
				'type' => 'local',
				'path' => 'style.css',
				'view' => 'all',
			],
		];
	}
	
}
